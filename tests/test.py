import unittest

from mi_suma import sum


class TestSum(unittest.TestCase):
    def test_list_int(self):
        """
        Test que suma una lista de números enteros
        """
        datos = [1, 2, 3]
        resultado = sum(datos)
        self.assertEqual(resultado, 6)


if __name__ == '__main__':
    unittest.main()
