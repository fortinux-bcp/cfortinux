# README #

This repository aims to test Atlassian devops as "groundwork to start shipping and operating software with a pre-configured Jira project called “Open DevOps” that combines Atlassian products and partner offerings. More information at https://www.atlassian.com/blog/devops/open-devops.

### What is this repository for? ###

* Quick summary: read above
* Version 0.0.1

### How do I get set up? ###

* Documentation IN PROGRESS...
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact